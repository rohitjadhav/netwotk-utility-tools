### About
___

**Network Utility Tools**
This is a suite of network utility tools.

**Version**
v1.1.0


### Setup  
___

#### List of pre-requisites  

* Node.js (>8.4.0)
* Net-SNMP (>=5.7.3)

#### Install pre-requisites  

```bash
# Below steps only if the machine has internet connectivity

# Step 1:
$ sudo apt-get update

# Step 2:
$ sudo apt-get install nodejs

# Step 3:
$ sudo apt-get install npm
```

#### Spin it up

```bash
# Step 1 (in case the machine has internet connectivity):
$ npm install

# Step 2:
# Place the input file in the following directory: network-utility-tools > input-files

# Step 3:
$ mkdir snmp-engine-output-files

# Step 4:
$ node name-of-the-engine.js
```


### Legal
___

##### Author: **Eywa**
/*
 * Imports
 */

const csv = require('fast-csv'),
    properties = require('./config/properties.json'),
    CONFIG = require('./config/config.js'),
    http = require('http'),
    fs = require('fs'),
    logger = require('log4js').configure(properties.logger).getLogger(),
    _ = require('lodash');

/*
 * Declarations
 */

const self = this;
let vendor = (CONFIG.vendor || properties.vendor).toLowerCase();
let market = (CONFIG.market || properties.market);
let parent_ip_oid = CONFIG.oids[vendor].parent_ip_oid || properties.oids[vendor].parent_ip_oid;
let freq_tx_oid = CONFIG.oids[vendor].freq_tx_oid || properties.oids[vendor].freq_tx_oid;
let freq_rx_oid = CONFIG.oids[vendor].freq_rx_oid || properties.oids[vendor].freq_rx_oid;
let modulation_oid = CONFIG.oids[vendor].modulation_oid || properties.oids[vendor].modulation_oid;
let all_slots = properties.exact_oids[vendor].slots;
let output_path = './snmp-engine-output-files/';

let inputData = [];
let links = [];

/*
 * MAIN
 */

let server = http.createServer();
server.on('error', function (e) {
    logger.error("Error: %j", e);
    console.log(e);
});
server.listen(properties.port, properties.host, () => {
    self.readFile();
    server.close();
});

/*
 * Functions
 */

self.readFile = function () {
    const inputFile = process.argv[2];
    logger.all(`Loading data from file: ${inputFile}`);
    let data = fs.readFileSync(inputFile);
    csv.fromString(data, { headers: true, renameHeaders: false, ignoreEmpty: true })
        .on("data", function (record) {
            self.formatInputData(record);
        })
        .on("end", function () {
            logger.all(`Completed formatting input data for all rows from file: ${inputFile}`);
            self.createLinks();
            self.saveResults(links, () => {
                return;
            });
        });
    return;
}

self.formatInputData = function(data) {
    const search = data["snmp-result"].split(" || ");
    let obj = {};
    obj["slots"] = {};
    obj["IP"] = data["IP"];
    let slot_obj = {};
    for (let key in all_slots) {
        let slot = all_slots[key];        
        slot_obj[slot] = {};
        search.forEach(element => {
            let regex;            
            if (element.includes(key)) { 
                regex = new RegExp(freq_tx_oid, "g");
                if (element.match(regex)) {
                    let split_str_freq_tx = element.split(":")[1].match(/[0-9]{1,7}.[0-9]{1,3}/g);
                    if (split_str_freq_tx) {
                        slot_obj[slot]['freq_tx'] = split_str_freq_tx[0];
                    } else {
                        slot_obj[slot]['freq_tx'] = 'na';
                    }
                    delete split_str_freq_tx;
                }
                regex = new RegExp(freq_rx_oid, "g");
                if (element.match(regex)) {
                    let split_str_freq_rx = element.split(":")[1].match(/[0-9]{1,7}.[0-9]{1,3}/g);
                    if (split_str_freq_rx) {  
                        slot_obj[slot]['freq_rx'] = split_str_freq_rx[0];
                    } else {
                        slot_obj[slot]['freq_rx'] = 'na';
                    }
                    delete split_str_freq_rx;
                }
                regex = new RegExp(modulation_oid, "g");
                if (element.match(regex)) {
                    let split_str_modulation = element.split(":")[1].match(/[0-9]{1,4}/g);
                    if (split_str_modulation) {
                        slot_obj[slot]['modulation'] = element.split(":")[1].toString();
                    } else {
                        slot_obj[slot]['modulation'] = 'na';
                    }
                    delete split_str_modulation;                
                }
            } else {
                regex = new RegExp(parent_ip_oid, "g");
                if (element.match(regex)) {
                    obj["parent_ip"] = element.split(":")[1].match(/(\d{1,3}\.){3}\d{1,3}/g)[0];
                }
            }
            delete regex;
        });
    }
    for (let key in slot_obj) {
        if (slot_obj[key].hasOwnProperty('freq_tx') || slot_obj[key].hasOwnProperty('freq_rx') || 
        slot_obj[key].hasOwnProperty('modulation')) {
            obj["slots"][key] = {};
            _.assign(obj["slots"][key], slot_obj[key]);
        }
    };
    inputData.push(obj);        
    return;
}

self.createLinks = function() {
    let inputData2 = inputData;
    inputData.forEach((element, i) => {
        inputData2.forEach(elem => {
            if (element.IP === elem.parent_ip) {
                let row = {};   
                row['nearIP'] = element.IP;
                row['farIP'] = elem.IP;
                let near_slot_array = [];
                let far_slot_array = [];
                let match_count = 0;
                for (let element_slot in element["slots"]) {
                    for (let elem_slot in elem["slots"]) {                        
                        if (
                            element["slots"][element_slot]["freq_tx"] !== 'na' &&
                            element["slots"][element_slot]["freq_rx"] !== 'na' &&
                            element["slots"][element_slot]["modulation"] !== 'na'
                        ) {
                            if (
                                element["slots"][element_slot]["freq_tx"] === elem["slots"][elem_slot]["freq_rx"] &&
                                element["slots"][element_slot]["freq_rx"] === elem["slots"][elem_slot]["freq_tx"] &&
                                element["slots"][element_slot]["modulation"] === elem["slots"][elem_slot]["modulation"]
                            ) {
                                match_count++;
                                near_slot_array.push(element_slot);
                                far_slot_array.push(elem_slot);
                            }                            
                        } else {
                            row['near_slot'] = 'cannot compute';
                            row['far_slot'] = 'cannot compute';
                        }
                    }
                }
                if (match_count == 1) {
                    row['near_slot'] = near_slot_array[0];
                    row['far_slot'] = far_slot_array[0];
                } else if (match_count == 0) {
                    row['near_slot'] = 'no match';
                    row['far_slot'] = 'no match';
                } else if (match_count > 1) {
                    row['near_slot'] = near_slot_array.join();
                    row['far_slot'] = far_slot_array.join();
                }
                links.push(row);                                
            }
        });        
    });
    return;
}

self.saveResults = function (all_results, cb) {
    let start_ts = new Date();
    let x = start_ts.toString().split(' ', 4);
    const today = x[3] + "-" + x[1] + "-" + x[2];
    let y = start_ts.toString().split(':');
    const now = y[0].toString().split(' ')[4] + y[1];
    const fileName = vendor.toUpperCase() + "_" + market.toUpperCase() + "-links-" + today + '_' + now + ".csv";

    csv.writeToString(all_results, { headers: true, quoteColumns: true }, function (err, csvdata) {
        if (err) {
            logger.error(`Unable to prepare CSV string. ${err}, err obj: ${self.inspect(err)}`);
            csvdata = "";
        }
        fs.writeFile(output_path + fileName, csvdata, (err) => {
            logger.all(`Finished saving file. err: ${err}`);
            return cb(err);
        });
    });
}

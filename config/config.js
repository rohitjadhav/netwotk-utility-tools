const self = this;

/*
 * Initial
 */ 

self.market = "KL";
self.vendor = "NEC";

/*
 * Input files
 */ 

self.input_file = "KL-NEC.csv";

/*
 * PING
 */

self.ping_query = {
    count_option: "-c",
    count_param: "3",
    timeout_option: "-t",
    timeout_param: "2",
    interface_option: "",
    interface_param: ""
};

/* 
 * SNMP
 */

self.snmp_command = "snmpwalk";
self.snmp_query = {
    timeout_option: "-t",
    timeout_param: "2",
    oid_format_option: "-On",
    version_option: "-v",
    version_param: "2c",
    community_option: "-c",
    community_param: "private",
    user_option: "-u",
    user_param: "admin_user",
    security_level_option: "-l",
    security_level_param: "authNoPriv",
    password_option: "-A",
    password_param: "ericsson"
};

self.oids = {
    nec: {
        community_string_oid: ".1.3.6.1.4.1.119.2.3.69.5.3.11.2.1",
        slot_mapping_oid : ".1.3.6.1.4.1.119.2.3.69.501.5.5.1.1.3",
        parent_ip_oid: ".1.3.6.1.4.1.119.2.3.69.5.1.1.1.8.1", 
        freq_tx_oid: ".1.3.6.1.4.1.119.2.3.69.501.4.2.1.3",
        freq_rx_oid: ".1.3.6.1.4.1.119.2.3.69.501.4.2.1.4",
        modulation_oid: ".1.3.6.1.4.1.119.2.3.69.501.8.1.1.13"
    }
};

exports.self;

/*
 * Imports
 */

const async = require('async'),
    csv = require('fast-csv'),
    properties = require('./config/properties.json'),
    logger = require('log4js').configure(properties.logger).getLogger(),
    fs = require('fs'),
    util = require('util'),
    exec = require('child_process').exec,
    CONFIG = require('./config/config.js'),
    http = require('http');

/*
 * Declarations
 */

const self = this;
let tasks = {};
let market = CONFIG.market || properties.market;
let vendor = (CONFIG.vendor || properties.vendor).toLowerCase();
let input_file = './input-files/' + (CONFIG.input_file || properties.input_file);
let output_path = './snmp-engine-output-files/';
let OIDs = CONFIG.oids[vendor] || properties.oids[vendor];
let snmp_command = CONFIG.snmp_command || properties.snmp_command;
let oid_format_option = CONFIG.snmp_query.oid_format_option || properties.snmp_query.oid_format_option;
let timeout_option = CONFIG.snmp_query.timeout_option || properties.snmp_query.timeout_option;
let timeout_param = CONFIG.snmp_query.timeout_param || properties.snmp_query.timeout_param;
let version_option = CONFIG.snmp_query.version_option || properties.snmp_query.version_option;
let version_param = CONFIG.snmp_query.version_param || properties.snmp_query.version_param;
let community_option = CONFIG.snmp_query.community_option || properties.snmp_query.community_option;
let community_param = CONFIG.snmp_query.community_param || properties.snmp_query.community_param;

/*
 * MAIN
 */

let server = http.createServer();
server.on('error', function (e) {
    logger.error("Error: %j", e);
    console.log(e);    
});
server.listen(properties.port, properties.host, () => {
    self.getIPs();
});

/*
 * Functions
 */

const validator = {
    isIP(ip) {
        return (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip));
    }
};

self.getIPs = function () {
    logger.all(`Loading IPs from file: ${input_file}`);
    let data = fs.readFileSync(input_file), // One time loading of the ips_file, done sync-ly
        ips = [];
    csv.fromString(data, { headers: true, renameHeaders: false, ignoreEmpty: true })
        .validate(function (data) {
            logger.all("Entered csv.validate().");
            return validator.isIP(data.IP);
        })
        .on("data-invalid", function (data) {
            logger.error("Invalid data: %j", data);
        })
        .on("data", function (record) {
            logger.all("Pushing %j to 'ips'", record);
            ips.push(record.IP);
        })
        .on("end", function (record_count) {
            logger.all("About to call getTasks().");
            self.getTasks(ips, function (err, res) {
                self.runTasks(tasks, function (err, res) {
                    self.saveResults(res, function (res) {
                        server.close();
                    });
                });
            });
        });
}

self.getTasks = function (ips, cb) {
    async.waterfall([
        function (wcb) {
            logger.all("In getTasks > 1st waterfall function.");
            self.addTasks(ips, function (res) {
                return wcb(res);
            });
        },
        function (wcb) {
            logger.all("In getTasks > 2nd waterfall function.");
            self.finalizeTasks(function (res) {
                return wcb(res);
            });
        }
    ], function (err, res) {
        logger.all("Exiting getTasks().");
        return cb(err, res);
    });
}

self.addTasks = function (ips, wcb) {
    async.forEach(ips, function (ip, fecb) {
        if (!tasks[ip]) tasks[ip] = {};
        tasks[ip] = {
            "IP": ip,
        };
        
        tasks[ip]["OIDs"] = OIDs;
        logger.all("Added task: %j", tasks[ip]);
        return fecb(null);
    }, function (err) {
        if (err) {
            logger.error("Error occurred during creation of task list. Err str: '%s'. Err Obj: %j.", err, err);
        }
        logger.all("%j", tasks);
        logger.all("Exiting addTasks().");
        return wcb(err);
    });
}

self.finalizeTasks = function (wcb) {
    let countIps = 0,
        countTasks = 0,
        new_tasks = [];

    async.forEach(Object.keys(tasks), function (ip, fecb) {
        let err = null;
        if (!Number.isSafeInteger(countIps + 1)) {
            err = "ERROR_MAX_SAFE_INTEGER";
            return fecb(err);
        }
        countIps++;
        let len = Object.keys(tasks[ip]).length;
        if (!Number.isSafeInteger(countTasks + len)) {
            logger.error("countTasks overflow when trying to increment countTasks");
            err = "ERROR_MAX_SAFE_INTEGER";
            return fecb(err);
        }
        countTasks += len;
        new_tasks.push(tasks[ip]);
        fecb(err);
    }, function (err) {
        if (err) {
            return wcb(err);
        }
        tasks = new_tasks;
        return wcb(err);
    });
}

self.runTasks = function (tasks, cb) {
    logger.all("Started runTasks().");
    let all_results = [];
    let q = async.queue(function (task, callback) {
        logger.all("Entered worker function of queue for task: %j", task);
        process.nextTick(() => (
            self.runIndividualTask(task, function (err, res) {
                logger.all("Entered runIndividualTask callback() for %j: ", res);
                if (Array.isArray(res)) all_results = all_results.concat(res);
                return callback(err, res);
            })
        ));
        logger.all("Exiting worker function of queue for task: %j", task);
    }, properties.concurrency_limit);
    q.drain = function () {
        logger.all("Exiting through qdrain");
        return cb(null, all_results);
    };
    logger.all("Adding 'tasks' to the queue.");
    q.push(tasks);
}

self.runIndividualTask = function (task, cb) {
    logger.all("Entered runIndividualTask for: %j", task);
    let results = [];
    let single_result = {
        IP: task.IP,
    };

    let snmpQueryString = "";

    let queryArray = [];
    for (const oid in Object.values(task.OIDs)) {
        const element = Object.values(task.OIDs)[oid];
        queryArray.push([
            snmp_command, oid_format_option, timeout_option, timeout_param,
            version_option, version_param, community_option, community_param, task.IP, element].join(" "));
    }
    snmpQueryString = queryArray.join(" && ");

    exec(snmpQueryString, function (error, stdout, stderr) {
        if (error) {
            single_result["snmp-result"] = stderr;
        }
        else {
            // let stdout = '.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.3.1 = STRING: "private"\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.4.1 = INTEGER: 3\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.5.1 = IpAddress: 10.151.12.2\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.6.1 = INTEGER: 32\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.7.1 = INTEGER: 1\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.8.1 = IpAddress: 0.0.0.0\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.9.1 = INTEGER: 0\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.10.1 = IpAddress: 0.0.0.0\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.11.1 = INTEGER: 0\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.12.1 = INTEGER: 2\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.13.1 = INTEGER: 1\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.14.1 = INTEGER: 1\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.15.1 = Hex-STRING: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.16.1 = INTEGER: 0\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.17.1 = Hex-STRING: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.18.1 = INTEGER: 0\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.19.1 = Hex-STRING: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.20.1 = INTEGER: 0\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.21.1 = INTEGER: 1\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.22.1 = INTEGER: 1\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.23.1 = INTEGER: 1\r\n.1.3.6.1.4.1.119.2.3.69.5.3.11.2.1.24.1 = INTEGER: 1\r\n.1.3.6.1.4.1.119.2.3.69.501.5.5.1.1.3.16842752 = Hex-STRING: 4B 4C 30 34 32 33 31 5F 49 42 53 2D 4D 41 4C 41 \r\n50 50 55 52 41 4D 20 4F 46 46 49 43 45 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.501.5.5.1.1.3.25231360 = Hex-STRING: 4B 4C 30 34 36 30 31 5F 4B 4F 4F 54 49 4C 41 4E \r\n47 41 44 49 00 00 00 00 00 00 00 00 00 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.501.5.5.1.1.3.33619968 = Hex-STRING: 4B 4C 30 30 30 34 30 5F 4D 41 4C 41 50 50 55 52 \r\n41 4D 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.501.5.5.1.1.3.42008576 = Hex-STRING: 4B 4C 30 30 30 34 30 5F 4D 41 4C 41 50 50 55 52 \r\n41 4D 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.501.5.5.1.1.3.50397184 = Hex-STRING: 4B 4C 30 32 31 31 39 5F 55 4E 4E 41 4D 54 48 41 \r\n4C 41 00 00 00 00 00 00 00 00 00 00 00 00 00 00 \r\n.1.3.6.1.4.1.119.2.3.69.5.1.1.1.8.1 = IpAddress: 10.151.200.1\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.3.16842752 = STRING: "21238.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.3.25231360 = STRING: "14956.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.3.33619968 = STRING: "14515.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.3.42008576 = STRING: "14515.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.3.50397184 = STRING: "21238.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.4.16842752 = STRING: "22470.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.4.25231360 = STRING: "15376.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.4.33619968 = STRING: "14935.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.4.42008576 = STRING: "14935.000"\r\n.1.3.6.1.4.1.119.2.3.69.501.4.2.1.4.50397184 = STRING: "22470.000"\r\n';
            let str = stdout.replace(/\r\n/g, ' || ');
            str = str.replace(/\n/g, ' || ');
            str = str.replace(/\"/g, "'");
            single_result["snmp-result"] = str;           
        }
        results.push(single_result);
        logger.info("%j", single_result);
        return cb(null, results);
    });
}

self.saveResults = function (all_results, cb) {
    let start_ts = new Date();
    let x = start_ts.toString().split(' ', 4);
    const today = x[3] + "-" + x[1] + "-" + x[2];
    let y = start_ts.toString().split(':');
    const now = y[0].toString().split(' ')[4] + y[1];
    const fileName = vendor + "_" + market + "-" + today + "_" + now + '.csv';

    csv.writeToString(all_results, { headers: true, quoteColumns: true }, function (err, csvdata) {
        if (err) {
            logger.error(`Unable to prepare CSV string. ${err}, err obj: ${self.inspect(err)}`);
            csvdata = "";
        }
        fs.writeFile(output_path + fileName, csvdata, (err) => {
            logger.all(`Finished saving file. err: ${err}`);
            return cb(err);
        });
    });
}

/* Util functions */

self.inspect = function (obj) {
    return util.inspect(obj, { showHidden: true, colors: false, depth: null });
}
